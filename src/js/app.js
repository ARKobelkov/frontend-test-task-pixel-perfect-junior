import $ from 'jquery';
import 'lazysizes';

$(function() {
  function initPremium() {
    const closeBtn = $('.js-premium-close');
    const praentBlock = closeBtn.parents('.js-premium');
  
    closeBtn.on('click', function() {
      praentBlock.slideUp();
    });
  }

  function initPlan() {
    const changeBtn = $('#changeBtn');
    const prices = $('.js-price');
    let randomPrice;

    function getRandom(min, max, fixed = 1) {
      return Number(Math.floor(Math.random() * (max - min + 1)) + min)+Number(Math.random().toFixed(fixed));
    }

    changeBtn.on('click', function() {
      randomPrice = getRandom(0, 50, 2);
      prices.each(function() {
        $(this).text(randomPrice);
      });
    });
  }

  function initNav() {
    const pull = $('#pull');
    const nav = $('#menu');
    const closeBtn = $('.js-nav-close');
    const body = $('.js-body');

    pull.on('click', function() {
      nav.addClass('_active');
      body.addClass('_frozen');
    });
    closeBtn.on('click', function() {
      nav.removeClass('_active');
      body.removeClass('_frozen');
    });
  }
 
  function initProgressbar() {
    const progressbars = $('.js-progressbar');
    progressbars.each(function() {
      const width = $(this).attr('data-width');
      $(this).css('width',`${width}%`);
    });
  }

  function initTabs() {
    $('#tabs').each(function () {
        let $wrapper = $(this);
        let $targetWrapper = $wrapper.find('.js-tabs__wrapper');
        let $triggers = $wrapper.find('.js-tabs__trigger[data-href]');
        if (!$triggers.length) {
            return;
        }
        if (!$triggers.filter('._active').length) {
            $triggers.first().addClass('_active');
        }
        $triggers.filter(':not(._active)').each(function () {
            $($(this).data('href')).hide();
        });
        $triggers.filter('._active').each(function () {
            $($(this).data('href')).addClass('_active');
        });
        $triggers.on('click', function () {
            if ($(this).hasClass('_active')) {
                return;
            }
            let href = $(this).data('href');
            let $target = $(href);
            if (!$target.length) {
                return;
            }
            $triggers.removeClass('_active');
            $(this).addClass('_active');
            let $parent = $(this).parent();
            $parent.animate({
                scrollLeft: $parent.scrollLeft() + $(this).position().left - parseInt($parent.css('padding-left')),
            });
            let $current = $wrapper.find('.js-tabs__target._active');
            $targetWrapper.css('height', $current.outerHeight());
            $current.fadeOut();
            $target.css({
                visibility: 'hidden',
                display: 'block'
            });
            let targetHeight = $target.outerHeight();
            $target.css({
                display: 'none',
                visibility: 'visible'
            });
            $targetWrapper.animate({ height: targetHeight }, () => {
                $target.fadeIn(() => {
                    $current.removeClass('_active');
                    $target.addClass('_active');
                    $targetWrapper.css('height', 'auto');
                });
            });
        });
    });
  }

  initPremium();
  initPlan();
  initNav();
  initProgressbar();
  initTabs();
});
